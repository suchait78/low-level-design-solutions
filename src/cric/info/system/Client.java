package cric.info.system;

import cric.info.system.functions.CricInfo;
import cric.info.system.functions.Observer;
import cric.info.system.functions.ProcessCricketData;
import cric.info.system.functions.SportWebsite;
import cric.info.system.models.*;

import java.util.Arrays;

public class Client {

    public static void main(String[] args){

        CricketServer server = new CricketServer();

        Match match1 = new Match(Location.LOCATION1, MatchType.COMPLETE);

        Team team1 = new Team(TeamName.INDIA);
        team1.addPlayer(new Player("player1",10,(double)1000));
        team1.addPlayer(new Player("player2",10,(double)1000));
        team1.addPlayer(new Player("player3",10,(double)1000));
        team1.addPlayer(new Player("player4",10,(double)1000));
        team1.addPlayer(new Player("player5",10,(double)1000));

        Team team2 = new Team(TeamName.SOUTH_AFRICA);

        team2.addPlayer(new Player("player6",10,(double)1000));
        team2.addPlayer(new Player("player7",10,(double)1000));
        team2.addPlayer(new Player("player8",10,(double)1000));
        team2.addPlayer(new Player("player9",10,(double)1000));
        team2.addPlayer(new Player("player10",10,(double)1000));

        match1.addTeam(team1);
        match1.addTeam(team2);
        match1.setTossWinner(TeamName.INDIA);
        match1.setCurrentInningRunning(1);

        Inning inning1 = new Inning();
        inning1.setCurrentBatsman(team1.getPlayerList().get(1));
        inning1.setCurrentBowler(team2.getPlayerList().get(2));
        inning1.setMatchId(match1.getMatchId());
        inning1.setTeamId(team1.getTeamId());
        inning1.setTotalBowls(3);
        inning1.setTotalWickets(0);
        inning1.setTotalRuns(6);

        match1.addInningsInPriorityMap(1, inning1);

        Observer observer1 = new CricInfo();

        server.addObserver(observer1);
        server.addMatch(match1);

        Observer observer2 = new SportWebsite();
        server.addObserver(observer2);

        ProcessCricketData data = new ProcessCricketData(Arrays.asList(match1), server);
        data.processMatchData();



    }
}

