package cric.info.system.functions;

import cric.info.system.models.Match;

import java.util.List;

public interface Observer {

    public void update(List<Match> matchList);
}
