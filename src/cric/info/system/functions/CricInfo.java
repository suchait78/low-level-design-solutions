package cric.info.system.functions;

import cric.info.system.models.Inning;
import cric.info.system.models.Match;
import cric.info.system.util.RandomIdGeneratorUtil;

import java.util.List;

public class CricInfo implements Observer, CricketOperations{

    private long observerId;

    public CricInfo() {
        this.observerId = RandomIdGeneratorUtil.getId();
    }

    @Override
    public void update(List<Match> matchList) {
        showDetails(matchList);
    }

    @Override
    public void showDetails(List<Match> matchList) {

        matchList.forEach(match -> {

            System.out.print("----------CricInfo Current Match Details----------");

            Inning currentInning = match.getInningsFromPriorityMap(match.getCurrentInningRunning());

            System.out.println(currentInning.getAverageRunRate());
            System.out.println(currentInning.getCurrentBatsman());
            System.out.println(currentInning.getCurrentBowler());
            System.out.println("Total Runs : " + currentInning.getTotalRuns());
            System.out.println("Total Bowls : " + currentInning.getTotalBowls());
            System.out.println("Total Wickets : " + currentInning.getTotalWickets());

            System.out.print("----------CricInfo Match Details End here---------");

        });
    }
}
