package cric.info.system.functions;

import cric.info.system.models.Match;

import java.util.List;

public interface CricketOperations {

    public void showDetails(List<Match> matchList);
}
