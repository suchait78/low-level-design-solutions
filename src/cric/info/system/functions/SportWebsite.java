package cric.info.system.functions;

import cric.info.system.models.Inning;
import cric.info.system.models.Match;
import cric.info.system.util.RandomIdGeneratorUtil;

import java.util.List;

public class SportWebsite implements Observer, CricketOperations{

    private long observerId;

    public SportWebsite() {
        this.observerId = RandomIdGeneratorUtil.getId();
    }

    @Override
    public void showDetails(List<Match> matchList) {
        showDetails(matchList);
    }

    @Override
    public void update(List<Match> matchList) {

        System.out.println();

        matchList.forEach(match -> {

            System.out.print("----------SportWebsite Current Match Details----------");

            Inning currentInning = match.getInningsFromPriorityMap(match.getCurrentInningRunning());

            System.out.println(currentInning.getAverageRunRate());
            System.out.println(currentInning.getCurrentBatsman());
            System.out.println(currentInning.getCurrentBowler());
            System.out.println("Total Runs : " + currentInning.getTotalRuns());
            System.out.println("Total Bowls : " + currentInning.getTotalBowls());
            System.out.println("Total Wickets : " + currentInning.getTotalWickets());

            System.out.print("----------SportWebsite Match Details End here---------");

        });
    }
}
