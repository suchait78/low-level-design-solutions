package cric.info.system.functions;

import cric.info.system.models.CricketServer;
import cric.info.system.models.Inning;
import cric.info.system.models.Match;

import java.util.List;

public class ProcessCricketData {

    private List<Match> matchList;
    private CricketServer server;

    public ProcessCricketData(List<Match> matchList, CricketServer server) {
        this.matchList = matchList;
        this.server = server;
    }

    public void processMatchData(){

        calculateAverageRunRate();
        server.updateObservers();
    }

    private void  calculateAverageRunRate(){

        matchList.forEach(match ->{
            Inning inning = match.getInningsFromPriorityMap(match.getCurrentInningRunning());
            inning.setAverageRunRate(inning.getTotalRuns()/inning.getTotalBowls());
        });
    }

    private List<Match> updateMatchData(){

        return null;
    }

}
