package cric.info.system.models;

import cric.info.system.functions.Observer;

import java.util.ArrayList;
import java.util.List;

public class CricketServer {

    private List<Match> currentMatchesList;
    private List<Match> totalMatchesList;
    private List<Observer> observerList;

    public CricketServer(){
        this.currentMatchesList = new ArrayList<>();
        this.totalMatchesList = new ArrayList<>();
        this.observerList = new ArrayList<>();
    }

    public void addMatch(Match match){
        this.currentMatchesList.add(match);
        this.totalMatchesList.add(match);
    }

    public void removeMatch(Match match){
        this.currentMatchesList.remove(match);
    }

    public void addObserver(Observer observer){
        synchronized (this){
            this.observerList.add(observer);
        }
    }

    public void removeObserver(Observer observer){
        synchronized (this){
            this.observerList.remove(observer);
        }
    }

    public void updateObservers(){
        observerList.forEach(observer ->observer.update(currentMatchesList));
    }
}
