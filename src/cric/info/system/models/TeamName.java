package cric.info.system.models;

public enum TeamName {

    INDIA("india"),
    PAKISTAN("pakistan"),
    AUSTRALIA("australia"),
    SOUTH_AFRICA("south_africa"),
    ENGLAND("england");

    private String countryName;

    TeamName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
