package cric.info.system.models;


import cric.info.system.util.RandomIdGeneratorUtil;

import java.util.ArrayList;
import java.util.List;

public class Team {

    private long teamId;
    private List<Player> playerList;
    private List<Coach> coachList;
    private TeamName teamName;
    private long totalInnings;

    public Team(TeamName teamName) {
        this.teamName = teamName;
        this.teamId = RandomIdGeneratorUtil.getId();
        this.playerList = new ArrayList<>();
        this.coachList = new ArrayList<>();
    }

    public long getTotalInnings() {
        return totalInnings;
    }

    public void setTotalInnings(long totalInnings) {
        this.totalInnings = totalInnings;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public void addPlayer(Player player){
        this.playerList.add(player);
    }

    public void removePlayer(Player player){
        this.playerList.remove(player);
    }

    public List<Coach> getCoachList() {
        return coachList;
    }

    public void setCoachList(List<Coach> coachList) {
        this.coachList = coachList;
    }

    public void addCoach(Coach coach){
        this.coachList.add(coach);
    }

    public void removeCoach(Coach coach){
        this.coachList.remove(coach);
    }

    public TeamName getTeamName() {
        return teamName;
    }

    public void setTeamName(TeamName teamName) {
        this.teamName = teamName;
    }
}
