package cric.info.system.models;

public enum Location {

    LOCATION1("location1"),
    LOCATION2("location2"),
    LOCATION3("location3"),
    LOCATION4("location4");

    private String location;

    Location(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
