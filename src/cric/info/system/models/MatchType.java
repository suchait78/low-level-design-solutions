package cric.info.system.models;

public enum MatchType {

    COMPLETE("full",50),
    TWENTY_TWENTY("twenty_twenty",20);

    private String matchType;
    private long totalOvers;

    MatchType(String matchType, long totalOvers) {
        this.matchType = matchType;
        this.totalOvers = totalOvers;
    }

    public String getMatchType() {
        return matchType;
    }

    public long getTotalOvers() {
        return totalOvers;
    }
}
