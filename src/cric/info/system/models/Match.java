package cric.info.system.models;

import cric.info.system.util.RandomIdGeneratorUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Match {

    private long matchId;
    private List<Team> teamsList;
    private Map<Long, Inning> inningsPriorityMap;
    private Location matchLocation;
    private TeamName tossWinner;
    private long totalOversMatch;
    private MatchType matchType;
    private long currentInningRunning;

    public Match(Location matchLocation, MatchType matchType) {
        this.matchId = RandomIdGeneratorUtil.getId();
        this.matchLocation = matchLocation;
        this.matchType = matchType;
        this.totalOversMatch = matchType.getTotalOvers();
        this.teamsList = new ArrayList<>();
        this.inningsPriorityMap = new LinkedHashMap<>();
    }

    public void addTeam(Team team){
        this.teamsList.add(team);
    }

    public void removeTeam(Team team){
        this.teamsList.remove(team);
    }

    public TeamName getTossWinner() {
        return tossWinner;
    }

    public void setTossWinner(TeamName tossWinner) {
        this.tossWinner = tossWinner;
    }

    public long getCurrentInningRunning() {
        return currentInningRunning;
    }

    public void setCurrentInningRunning(long currentInningRunning) {
        this.currentInningRunning = currentInningRunning;
    }

    public long getMatchId() {
        return matchId;
    }

    public void setMatchId(long matchId) {
        this.matchId = matchId;
    }

    public List<Team> getTeamsList() {
        return teamsList;
    }

    public void setTeamsList(List<Team> teamsList) {
        this.teamsList = teamsList;
    }

    public void addInningsInPriorityMap(long priority, Inning inning) {
        this.inningsPriorityMap.put(priority, inning);
    }

    public void removeInningsInPriorityMap(long priority, Inning inning) {
        this.inningsPriorityMap.remove(priority);
    }

    public Inning getInningsFromPriorityMap(long currentInningRunning){
        return this.inningsPriorityMap.get(currentInningRunning);
    }

    public Location getMatchLocation() {
        return matchLocation;
    }

    public void setMatchLocation(Location matchLocation) {
        this.matchLocation = matchLocation;
    }
}
