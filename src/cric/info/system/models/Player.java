package cric.info.system.models;

import cric.info.system.util.RandomIdGeneratorUtil;

import java.util.Objects;

public class Player {

    private long playerId;
    private String playerName;
    private long playerTotalMatches;
    private double playerTotalRuns;


    public Player(String playerName, long playerTotalMatches, double playerTotalRuns) {
        this.playerId = RandomIdGeneratorUtil.getId();
        this.playerName = playerName;
        this.playerTotalMatches = playerTotalMatches;
        this.playerTotalRuns = playerTotalRuns;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public long getPlayerTotalMatches() {
        return playerTotalMatches;
    }

    public void setPlayerTotalMatches(long playerTotalMatches) {
        this.playerTotalMatches = playerTotalMatches;
    }

    public double getPlayerTotalRuns() {
        return playerTotalRuns;
    }

    public void setPlayerTotalRuns(double playerTotalRuns) {
        this.playerTotalRuns = playerTotalRuns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return playerId == player.playerId &&
                playerTotalMatches == player.playerTotalMatches &&
                Double.compare(player.playerTotalRuns, playerTotalRuns) == 0 &&
                Objects.equals(playerName, player.playerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, playerName, playerTotalMatches, playerTotalRuns);
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerId=" + playerId +
                ", playerName='" + playerName + '\'' +
                ", playerTotalMatches=" + playerTotalMatches +
                ", playerTotalRuns=" + playerTotalRuns +
                '}';
    }
}
