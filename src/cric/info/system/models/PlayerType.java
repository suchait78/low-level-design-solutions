package cric.info.system.models;

public enum PlayerType {

    BATSMAN("batsman"),
    SPEED_BOWLER("speed_bowler"),
    SPIN_BOWLER("spin_bowler");

    private String playerType;

    PlayerType(String playerType) {
        this.playerType = playerType;
    }

    public String getPlayerType() {
        return playerType;
    }
}
