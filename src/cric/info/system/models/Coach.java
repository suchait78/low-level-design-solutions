package cric.info.system.models;

import cric.info.system.util.RandomIdGeneratorUtil;

public class Coach {

    private long coachId;
    private String coachName;

    public Coach(long coachId, String coachName) {
        this.coachId = RandomIdGeneratorUtil.getId();
        this.coachName = coachName;
    }

    public long getCoachId() {
        return coachId;
    }

    public void setCoachId(long coachId) {
        this.coachId = coachId;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }
}
