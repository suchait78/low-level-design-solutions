package cric.info.system.models;

import cric.info.system.util.RandomIdGeneratorUtil;

import java.util.Objects;
import java.util.Random;

public class Inning {

    private long inningId;
    private double totalRuns;
    private double totalBowls;
    private double averageRunRate;
    private long totalWickets;
    private Player currentBatsman;
    private Player currentBowler;
    private long teamId;
    private long matchId;

    public Inning() {
        this.inningId = RandomIdGeneratorUtil.getId();
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public long getMatchId() {
        return matchId;
    }

    public void setMatchId(long matchId) {
        this.matchId = matchId;
    }

    public long getInningId() {
        return inningId;
    }

    public void setInningId(long inningId) {
        this.inningId = inningId;
    }

    public double getTotalRuns() {
        return totalRuns;
    }

    public void setTotalRuns(double totalRuns) {
        this.totalRuns = totalRuns;
    }

    public double getTotalBowls() {
        return totalBowls;
    }

    public void setTotalBowls(double totalBowls) {
        this.totalBowls = totalBowls;
    }

    public double getAverageRunRate() {
        return averageRunRate;
    }

    public void setAverageRunRate(double averageRunRate) {
        this.averageRunRate = averageRunRate;
    }

    public long getTotalWickets() {
        return totalWickets;
    }

    public void setTotalWickets(long totalWickets) {
        this.totalWickets = totalWickets;
    }

    public Player getCurrentBatsman() {
        return currentBatsman;
    }

    public void setCurrentBatsman(Player currentBatsman) {
        this.currentBatsman = currentBatsman;
    }

    public Player getCurrentBowler() {
        return currentBowler;
    }

    public void setCurrentBowler(Player currentBowler) {
        this.currentBowler = currentBowler;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Inning inning = (Inning) o;
        return inningId == inning.inningId &&
                Double.compare(inning.totalRuns, totalRuns) == 0 &&
                Double.compare(inning.totalBowls, totalBowls) == 0 &&
                Double.compare(inning.averageRunRate, averageRunRate) == 0 &&
                totalWickets == inning.totalWickets &&
                Objects.equals(currentBatsman, inning.currentBatsman) &&
                Objects.equals(currentBowler, inning.currentBowler);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inningId, totalRuns, totalBowls, averageRunRate, totalWickets, currentBatsman, currentBowler);
    }
}
