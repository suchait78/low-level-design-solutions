package hotel.booking.system;

import java.util.List;

public class Booking {

    private List<RoomBooking> roomsList;
    private int totalBookingPrice;
    private Coupon couponApplied;
    private boolean amountPaid;

    public Booking(List<RoomBooking> roomsList, Coupon couponApplied) {
        this.roomsList = roomsList;
        this.couponApplied = couponApplied;
    }

    public List<RoomBooking> getRoomsList() {
        return roomsList;
    }

    public void setRoomsList(List<RoomBooking> roomsList) {
        this.roomsList = roomsList;
    }

    public int getTotalBookingPrice() {
        return totalBookingPrice;
    }

    public void setTotalBookingPrice(int totalBookingPrice) {
        this.totalBookingPrice = totalBookingPrice;
    }

    public Coupon getCouponApplied() {
        return couponApplied;
    }

    public void setCouponApplied(Coupon couponApplied) {
        this.couponApplied = couponApplied;
    }

    public boolean isAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(boolean amountPaid) {
        this.amountPaid = amountPaid;
    }
}
