package hotel.booking.system;

public class BookingDataValidation {

    static void validateData(RoomBooking roomBooking, Hotel hotel) throws UserException, HotelException {

        if(roomBooking.getTotalUsers().size() == 0){
            throw new UserException("This booking doesn't have any user involved, incorrect data.");
        }

        if(roomBooking.getTotalUsers().size() > 2) {
            throw new UserException("Not more than 2 users can book one room, please book another");
        }

        if(roomBooking.getTotalDays() == 0) {
            throw new UserException("Total booking days cannot be zero, incorrect data.");
        }

        if(hotel.getTotalDeluxeRooms() == 0 || hotel.getTotalExecutiveRooms() == 0){
            throw new HotelException("Total rooms in hotel is 0, incorrect data.");
        }

        if(roomBooking.getRoomType().getRoomType().equals(RoomType.DELUXE.getRoomType())){

            if(hotel.getTotalAvailableDeluxeRooms() == 0){
                throw new HotelException("Sorry! none of the deluxe room is available - we can't book.");
            }
        }else if(roomBooking.getRoomType().getRoomType().equals(RoomType.EXECUTIVE.getRoomType())){

            if(hotel.getTotalExecutiveRooms() == 0){
                throw new HotelException("Sorry! none of the executive room is available - we can't book.");
            }
        }

    }
}
