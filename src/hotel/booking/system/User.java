package hotel.booking.system;

public class User {

    private String userName;
    private int userAge;
    private String userID;
    private String userAddress;
    private String userPhoneNumber;
    private PaymentMethod paymentMethod;

    public User(String userName, int userAge, String userID, String userAddress, String userPhoneNumber, PaymentMethod paymentMethod) {
        this.userName = userName;
        this.userAge = userAge;
        this.userID = userID;
        this.userAddress = userAddress;
        this.userPhoneNumber = userPhoneNumber;
        this.paymentMethod = paymentMethod;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }
}

