package hotel.booking.system;

public enum PaymentMethod {

    CARD("card"),CASH("cash"),PAYTM("paytm"),GPAY("google pay");

    private String paymentMethod;

    private PaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }
}
