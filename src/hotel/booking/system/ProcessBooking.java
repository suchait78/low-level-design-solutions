package hotel.booking.system;

import java.awt.print.Book;
import java.util.List;

public class ProcessBooking {

    private Booking booking;
    private RoomBooking roomBooking;

    private List<RoomBooking> processRoomBooking(Booking booking, Hotel hotel) throws UserException, HotelException {

        if(null == booking.getRoomsList()){
            throw new HotelException("There is no room exist in this booking, incorrect data.");
        }

        List<RoomBooking> roomBookingList = booking.getRoomsList();

        for(RoomBooking roomBooking : roomBookingList){

            BookingDataValidation.validateData(roomBooking, hotel);
            roomBooking.setTotalRoomPrice(calculateTotalPriceForRoom(roomBooking));
        }

        return roomBookingList;
    }

    public int processBooking(Booking booking, Hotel hotel) throws HotelException, UserException {

        boolean isValidCoupon = false;

        if(booking.getCouponApplied() != null) {

            Coupon bookingCoupon = booking.getCouponApplied();
            isValidCoupon = bookingCoupon.isValid();

            if (!isValidCoupon) {
                System.out.print("This coupon has expired so no amount will deduced from total amount.");
            }
        }

        List<RoomBooking> roomBookingList = processRoomBooking(booking,hotel);

        int totalBookingPrice = 0;

        for(RoomBooking roomBooking : booking.getRoomsList()){
            totalBookingPrice = totalBookingPrice + roomBooking.getTotalRoomPrice();
        }

        if(isValidCoupon){
            totalBookingPrice = totalBookingPrice - booking.getCouponApplied().getCouponDeductionAmount();
        }

        return totalBookingPrice;
    }

    private int calculateTotalPriceForRoom(RoomBooking roomBooking){
        return roomBooking.getRoomType().getRoomPrice() * roomBooking.getTotalDays();
    }
}
