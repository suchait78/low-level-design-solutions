package hotel.booking.system;

public class CouponException extends Exception{

    private String message;

    public CouponException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
