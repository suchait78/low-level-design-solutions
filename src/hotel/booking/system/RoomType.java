package hotel.booking.system;

public enum RoomType {

    DELUXE("deluxe",3000), EXECUTIVE("executive",4000);

    private String roomType;
    private int roomPrice;

    private RoomType(String roomType, int roomPrice){
        this.roomType = roomType;
        this.roomPrice = roomPrice;
    }

    public String getRoomType() {
        return roomType;
    }

    public int getRoomPrice() {
        return roomPrice;
    }
}
