package hotel.booking.system;

public enum Coupon {

    COUPON_ABC("test123",true, 500), COUPON_XYZ("test456",false, 600);

    private String couponName;
    private boolean isValid;
    private int couponDeductionAmount;

    private Coupon(String couponName, boolean isValid, int couponDeductionAmount) {
        this.couponName = couponName;
        this.isValid = isValid;
        this.couponDeductionAmount = couponDeductionAmount;
    }

    public String getCouponName() {
        return couponName;
    }

    public boolean isValid() {
        return isValid;
    }

    public int getCouponDeductionAmount() {
        return couponDeductionAmount;
    }
}
