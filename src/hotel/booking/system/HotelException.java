package hotel.booking.system;

public class HotelException extends Exception{

    private String message;

    public HotelException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
