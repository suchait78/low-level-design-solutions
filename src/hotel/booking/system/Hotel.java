package hotel.booking.system;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private int totalDeluxeRooms;
    private int totalExecutiveRooms;
    private int totalAvailableDeluxeRooms;
    private int getTotalAvailableExecutiveRooms;
    private List<Booking> totalBookings;

    public Hotel(int totalDeluxeRooms, int totalExecutiveRooms) {
        this.totalDeluxeRooms = totalDeluxeRooms;
        this.totalExecutiveRooms = totalExecutiveRooms;
        this.totalAvailableDeluxeRooms = totalDeluxeRooms;
        this.getTotalAvailableExecutiveRooms = totalExecutiveRooms;
        this.totalBookings = new ArrayList<>();
    }

    public int getTotalDeluxeRooms() {
        return totalDeluxeRooms;
    }

    public void setTotalDeluxeRooms(int totalDeluxeRooms) {
        this.totalDeluxeRooms = totalDeluxeRooms;
    }

    public int getTotalExecutiveRooms() {
        return totalExecutiveRooms;
    }

    public void setTotalExecutiveRooms(int totalExecutiveRooms) {
        this.totalExecutiveRooms = totalExecutiveRooms;
    }

    public int getTotalAvailableDeluxeRooms() {
        return totalAvailableDeluxeRooms;
    }

    public void setTotalAvailableDeluxeRooms(int totalAvailableDeluxeRooms) {
        this.totalAvailableDeluxeRooms = totalAvailableDeluxeRooms;
    }

    public int getGetTotalAvailableExecutiveRooms() {
        return getTotalAvailableExecutiveRooms;
    }

    public void setGetTotalAvailableExecutiveRooms(int getTotalAvailableExecutiveRooms) {
        this.getTotalAvailableExecutiveRooms = getTotalAvailableExecutiveRooms;
    }

    public List<Booking> getTotalBookings() {
        return totalBookings;
    }

    public void setTotalBookings(List<Booking> totalBookings) {
        this.totalBookings = totalBookings;
    }

    public void addBookingInBookingList(Booking booking){
        this.totalBookings.add(booking);
    }

}
