package hotel.booking.system;

import java.awt.print.Book;
import java.util.List;

public class ProcessHotel {

    public Hotel processHotel(Hotel hotel, Booking booking) throws HotelException {

        Hotel hotel1 = processHotelRoomsDeducation(hotel,booking);
        return addBookingToHotelRecord(hotel1,booking);
    }

    private Hotel processHotelRoomsDeducation(Hotel hotel, Booking booking) throws HotelException {

        if (null == booking.getRoomsList()) {
            throw new HotelException("There is no room exist in this booking, incorrect data.");
        }

        List<RoomBooking> roomBookingList = booking.getRoomsList();

        int totalDeluxeRoomsInBooking = (int)booking.getRoomsList().stream()
                .filter(room -> room.getRoomType().getRoomType().equals(RoomType.DELUXE.getRoomType())).count();

        int totalExecutiveRoomsInBooking = (int)booking.getRoomsList().stream()
                .filter(room -> room.getRoomType().getRoomType().equals(RoomType.EXECUTIVE.getRoomType())).count();

        int totalHotelDeluxeRooms = hotel.getTotalDeluxeRooms();
        int totalHotelExecutiveRooms = hotel.getTotalExecutiveRooms();

        synchronized (this){
            hotel.setTotalAvailableDeluxeRooms(totalHotelDeluxeRooms - totalDeluxeRoomsInBooking);
            hotel.setGetTotalAvailableExecutiveRooms(totalHotelExecutiveRooms - totalExecutiveRoomsInBooking);
        }

        return hotel;
    }

    private Hotel addBookingToHotelRecord(Hotel hotel, Booking booking){

        synchronized (this){
            hotel.addBookingInBookingList(booking);
        }

        return hotel;
    }

}
