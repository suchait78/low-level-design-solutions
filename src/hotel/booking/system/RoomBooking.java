package hotel.booking.system;

import java.util.ArrayList;
import java.util.List;

public class RoomBooking {

    private RoomType roomType;
    private int roomsCount;
    private int totalRoomPrice;
    private List<User> totalUsers;
    private boolean breakfastInclusion;
    private int totalDays;

    public RoomBooking(RoomType roomType, boolean breakfastInclusion, int totalDays) {
        this.roomType = roomType;
        this.breakfastInclusion = breakfastInclusion;
        this.totalDays = totalDays;
        this.totalUsers = new ArrayList<>();
    }

    public int getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(int totalDays) {
        this.totalDays = totalDays;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public int getRoomsCount() {
        return roomsCount;
    }

    public void setRoomsCount(int roomsCount) {
        this.roomsCount = roomsCount;
    }

    public int getTotalRoomPrice() {
        return totalRoomPrice;
    }

    public void setTotalRoomPrice(int totalRoomPrice) {
        this.totalRoomPrice = totalRoomPrice;
    }

    public List<User> getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(List<User> totalUsers) {
        this.totalUsers = totalUsers;
    }

    public void addUserInTotalUsersList(User user){
        this.totalUsers.add(user);
    }

}
