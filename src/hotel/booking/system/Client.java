package hotel.booking.system;

import java.awt.print.Book;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Client {

    public static void main(String[] args) throws HotelException, UserException {

        Hotel hotel = new Hotel(10,10);

        User user1 = new User("Suchait",28,"PQR45TEST","Gwalior"
                ,"9823742662",PaymentMethod.CARD);

        User user2 = new User("Test",28,"PQR45TEST1","Gwalior"
                ,"9823742662",PaymentMethod.CARD);

        RoomBooking roomBooking = new RoomBooking(RoomType.EXECUTIVE,true,2);
        roomBooking.addUserInTotalUsersList(user1);
        roomBooking.setRoomsCount(1);

        RoomBooking roomBooking1 = new RoomBooking(RoomType.DELUXE, true, 2);
        roomBooking1.addUserInTotalUsersList(user2);
        roomBooking1.setRoomsCount(2);

        List<RoomBooking> roomBookingList = new ArrayList<>();
        roomBookingList.add(roomBooking);
        roomBookingList.add(roomBooking1);

        Booking booking = new Booking(roomBookingList, Coupon.COUPON_ABC);

        ProcessBooking processBooking = new ProcessBooking();
        int totalAmountPayable = processBooking.processBooking(booking,hotel);

        System.out.println("Total Amount payable for booking : " + totalAmountPayable);

        booking.setAmountPaid(true);

        if(booking.isAmountPaid()) {
            //process hotel
            ProcessHotel processHotel = new ProcessHotel();
            processHotel.processHotel(hotel, booking);
        }

        System.out.println("Total available deluxe rooms : " + hotel.getTotalAvailableDeluxeRooms());
        System.out.println("Total available deluxe rooms : " + hotel.getGetTotalAvailableExecutiveRooms());

    }
}
