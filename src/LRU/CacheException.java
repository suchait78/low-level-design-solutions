package LRU;

public class CacheException extends Exception{

    private String message;

    public CacheException(String message) {
        super(message);
    }
}
