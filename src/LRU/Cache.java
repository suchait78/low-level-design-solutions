package LRU;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

public class Cache {

    private Map<Integer, Map.Entry<Integer, String>> cacheMap;
    private Integer cacheLimit;

    LinkedList<Map.Entry<Integer, String>> elementsList;

    public Cache(Integer cacheLimit) {
        this.cacheLimit = cacheLimit;
        this.cacheMap = new Hashtable<>(cacheLimit);
        this.elementsList = new LinkedList<>();
    }

    public void putEntry(Integer key, String value){

        if(this.cacheMap.size() == this.cacheLimit){
            evict();
        }

        insertList(key, value);
    }

    public void evict(){
        this.elementsList.removeFirst();
    }


    public String getEntry(Integer key) throws CacheException {

        if(this.cacheMap.size() == 0){
            throw new CacheException("No element is present in the cache.");
        }

        Map.Entry currentEntry = this.cacheMap.get(key);

        if(this.elementsList.remove(currentEntry)){
            this.elementsList.add(currentEntry);
        }

        return String.valueOf(currentEntry.getValue());
    }

    public void insertList(Integer key, String value){

        CacheDataEntry dataEntry = new CacheDataEntry<>(key, value);
        this.elementsList.add(dataEntry);
        this.cacheMap.put(key, dataEntry);
    }

   /* public String testMethod(){
        return this.elementsList.getLast().getValue();
    }

    public String testMethod1(){
        return this.elementsList.getFirst().getValue();
    }*/

}
