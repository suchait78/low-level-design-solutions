package LRU;

import java.util.LinkedList;

public class Client {

    public static void main(String[] args) throws CacheException {

        Cache cache = new Cache(3);

        cache.putEntry(2, "test1");
        cache.putEntry(5,"test5");
        cache.putEntry(13,"test13");

        //System.out.println(cache.testMethod());
        System.out.println(cache.getEntry(5));
        //System.out.println(cache.testMethod());

        cache.getEntry(2);
        cache.putEntry(67,"test67");

        //System.out.println(cache.testMethod());
        //System.out.println(cache.testMethod1());

    }

}
