package parking.lot.system.models;

import java.util.*;

public class ParkingFloor {

    private long parkingNumber;
    private long totalParkingCapacity;
    private long totalActiveVehicleEntries;
    private long totalVacantVehicleEntries;
    private List<Parking> parkingList;
    private HashMap<Spot, Long> totalSpotsMap;
    private List<Attendant> attendantList;


    public ParkingFloor(long parkingNumber, long totalParkingCapacity) {
        this.parkingNumber = parkingNumber;
        this.totalParkingCapacity = totalParkingCapacity;
        this.totalVacantVehicleEntries = totalParkingCapacity;
        this.parkingList = new ArrayList<>();
        this.attendantList = new ArrayList<>();
        this.totalSpotsMap = new HashMap<>();
    }

    public long getParkingNumber() {
        return parkingNumber;
    }

    public void setParkingNumber(long parkingNumber) {
        this.parkingNumber = parkingNumber;
    }

    public long getTotalParkingCapacity() {
        return totalParkingCapacity;
    }

    public void setTotalParkingCapacity(long totalParkingCapacity) {
        this.totalParkingCapacity = totalParkingCapacity;
    }

    public long getTotalActiveVehicleEntries() {
        return totalActiveVehicleEntries;
    }

    public void setTotalActiveVehicleEntries(long totalActiveVehicleEntries) {
        this.totalActiveVehicleEntries = totalActiveVehicleEntries;
    }

    public long getTotalVacantVehicleEntries() {
        return totalVacantVehicleEntries;
    }

    public void setTotalVacantVehicleEntries(long totalVacantVehicleEntries) {
        this.totalVacantVehicleEntries = totalVacantVehicleEntries;
    }

    public List<Parking> getParkingList() {
        return parkingList;
    }

    public void setParkingList(List<Parking> parkingList) {
        this.parkingList = parkingList;
    }

    public void addParkingInParkingList(Parking parking) {
        this.parkingList.add(parking);
    }

    public void removeParkingInParkingList(Parking parking) {
        this.parkingList.remove(parking);
    }

    public List<Attendant> getAttendantList() {
        return attendantList;
    }

    public void setAttendantList(List<Attendant> attendantList) {
        this.attendantList = attendantList;
    }

    public void addAttendantToAttenantList(Attendant attendant) {
        this.attendantList.add(attendant);
    }

    public void removeAttendantToAttenantList(Attendant attendant) {
        this.attendantList.remove(attendant);
    }

    public HashMap<Spot, Long> getTotalSpotsMap() {
        return totalSpotsMap;
    }

    public void addSpotInTotalSpotsMap(Spot spot, Long totalNumberSpots) {
        this.totalSpotsMap.put(spot, totalNumberSpots);
    }

    public void removeSpotInTotalSpotsMap(SpotType spotType, Long totalNumberSpots) {
        this.totalSpotsMap.remove(spotType);
    }

    public Long getRemainingSpots(SpotType spotType) {
        return this.totalSpotsMap.get(spotType);
    }

    public Map.Entry<Spot, Long> getEntryRemainingSpot(SpotType spotType) {
        return this.getTotalSpotsMap().entrySet().stream().filter(entry ->
                entry.getKey().getSpotType().equals(spotType)).findFirst().get();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingFloor that = (ParkingFloor) o;
        return parkingNumber == that.parkingNumber &&
                totalParkingCapacity == that.totalParkingCapacity &&
                totalActiveVehicleEntries == that.totalActiveVehicleEntries &&
                totalVacantVehicleEntries == that.totalVacantVehicleEntries &&
                Objects.equals(parkingList, that.parkingList) &&
                Objects.equals(totalSpotsMap, that.totalSpotsMap) &&
                Objects.equals(attendantList, that.attendantList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parkingNumber, totalParkingCapacity, totalActiveVehicleEntries, totalVacantVehicleEntries, parkingList, totalSpotsMap, attendantList);
    }

}
