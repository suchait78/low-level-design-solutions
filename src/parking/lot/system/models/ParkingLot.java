package parking.lot.system.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParkingLot {

    private long totalParkingCapacity;
    private long totalParkedVehicles;
    private long totalParkingFloors;
    private List<Parking> totalParkingsBookedList;
    private Map<Long, ParkingFloor> totalParkingFloorsMap;
    private List<Attendant> attendantList;
    private long currentActiveParkingNumber;

    public ParkingLot(long totalParkingFloors) {
        this.totalParkingFloors = totalParkingFloors;
        this.currentActiveParkingNumber = 1;

        this.attendantList = new ArrayList<>();
        this.totalParkingsBookedList = new ArrayList<>();
        this.totalParkingFloorsMap = new HashMap<>();
    }

    public long getTotalParkingCapacity() {
        return totalParkingCapacity;
    }

    public void setTotalParkingCapacity(long totalParkingCapacity) {
        this.totalParkingCapacity = totalParkingCapacity;
    }

    public long getTotalParkedVehicles() {
        return totalParkedVehicles;
    }

    public void setTotalParkedVehicles(long totalParkedVehicles) {
        this.totalParkedVehicles = totalParkedVehicles;
    }

    public long getTotalParkingFloors() {
        return totalParkingFloors;
    }

    public void setTotalParkingFloors(long totalParkingFloors) {
        this.totalParkingFloors = totalParkingFloors;
    }

    public List<Parking> getTotalParkingsBookedList() {
        return totalParkingsBookedList;
    }

    public void setTotalParkingsBookedList(List<Parking> totalParkingsBookedList) {
        this.totalParkingsBookedList = totalParkingsBookedList;
    }

    public void addParkingInParkingsBookedList(Parking parking) {
        this.totalParkingsBookedList.add(parking);
    }

    public void removeParkingInTotalParkingsBookedList(Parking parking) {
        this.totalParkingsBookedList.remove(parking);
    }

    public List<Attendant> getAttendantList() {
        return attendantList;
    }

    public void setAttendantList(List<Attendant> attendantList) {
        this.attendantList = attendantList;
    }

    public void addAttendantInAttendantList(Attendant attendant) {
        this.attendantList.add(attendant);
    }

    public void removeAttendantInAttendantList(Attendant attendant) {
        this.attendantList.remove(attendant);
    }

    public Map<Long, ParkingFloor> getTotalParkingFloorsMap() {
        return totalParkingFloorsMap;
    }

    public long getCurrentActiveParkingNumber() {
        return currentActiveParkingNumber;
    }

    public void setCurrentActiveParkingNumber(long currentActiveParkingNumber) {
        this.currentActiveParkingNumber = currentActiveParkingNumber;
    }

    public void addParkingFloorInTotalParkingFloorsMap(Long parkingNumber, ParkingFloor parkingFloor) {

        this.totalParkingCapacity = this.totalParkingCapacity + parkingFloor.getTotalParkingCapacity();
        totalParkingFloorsMap.put(parkingNumber, parkingFloor);
    }

    public void removeParkingFloorInTotalParkingFloorsMap(Long parkingNumber, ParkingFloor parkingFloor) {
        totalParkingFloorsMap.remove(parkingNumber);
    }

    public ParkingFloor getCurrentParkingFloor(Long currentParkingFloorNumber) {
        return this.totalParkingFloorsMap.get(currentParkingFloorNumber);
    }

}
