package parking.lot.system.models;

public enum SpotType {

    COMPACT("compact"),
    LARGE("large"),
    HANDICAPPED("handicapped"),
    MOTOR_CYCLE("motorcycle"),
    ELECTRIC("electric");

    private String spotType;

    private SpotType(String spotType) {
        this.spotType = spotType;
    }

    public String getSpotType() {
        return spotType;
    }
}
