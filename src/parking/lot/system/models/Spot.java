package parking.lot.system.models;

import java.util.Objects;

public class Spot {

    private SpotType spotType;
    private long totalSpotsInParkingFloor;

    public Spot(SpotType spotType, long totalSpotsInParkingFloor) {
        this.spotType = spotType;
        this.totalSpotsInParkingFloor = totalSpotsInParkingFloor;
    }

    public SpotType getSpotType() {
        return spotType;
    }

    public void setSpotType(SpotType spotType) {
        this.spotType = spotType;
    }

    public long getTotalSpotsInParkingFloor() {
        return totalSpotsInParkingFloor;
    }

    public void setTotalSpotsInParkingFloor(long totalSpotsInParkingFloor) {
        this.totalSpotsInParkingFloor = totalSpotsInParkingFloor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Spot spot = (Spot) o;
        return totalSpotsInParkingFloor == spot.totalSpotsInParkingFloor &&
                spotType == spot.spotType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(spotType, totalSpotsInParkingFloor);
    }

}
