package parking.lot.system.models;

public enum VehicleSupported {

    NON_ELECTRIC_CAR("non_electric_car", 100),
    ELECTRIC_CAR("electric_car", 150),
    BIKE("bike", 50),
    TRUCK("truck", 200);

    private String vehicle;
    private long parkingPrice;

    VehicleSupported(String vehicle, long parkingPrice) {
        this.vehicle = vehicle;
        this.parkingPrice = parkingPrice;
    }

    public String getVehicle() {
        return vehicle;
    }

    public long getParkingPrice() {
        return parkingPrice;
    }
}
