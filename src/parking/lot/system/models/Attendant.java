package parking.lot.system.models;

import java.util.ArrayList;
import java.util.List;

public class Attendant {

    private long attendantId;
    private String attendantName;
    private List<Parking> totalParkingProcessed;

    public Attendant(long attendantId, String attendantName) {
        this.attendantId = attendantId;
        this.attendantName = attendantName;
        this.totalParkingProcessed = new ArrayList<>();
    }

    public long getAttendantId() {
        return attendantId;
    }

    public void setAttendantId(long attendantId) {
        this.attendantId = attendantId;
    }

    public String getAttendantName() {
        return attendantName;
    }

    public void setAttendantName(String attendantName) {
        this.attendantName = attendantName;
    }

    public List<Parking> getTotalParkingProcessed() {
        return totalParkingProcessed;
    }

    public void setTotalParkingProcessed(List<Parking> totalParkingProcessed) {
        this.totalParkingProcessed = totalParkingProcessed;
    }

    public void addParkingToAttendantParkingProcessedList(Parking parking) {
        this.totalParkingProcessed.add(parking);
    }

    public void removeParkingToAttendantParkingProcessedList(Parking parking) {
        this.totalParkingProcessed.remove(parking);
    }

}
