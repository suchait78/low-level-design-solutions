package parking.lot.system.models;

public enum BillingLocations {

    EXIT("exit"), CUSTOMER_INFO_PORTAL("customerinfoportal");

    private String billingLocations;

    private BillingLocations(String billingLocations) {
        this.billingLocations = billingLocations;
    }
}
