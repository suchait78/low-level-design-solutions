package parking.lot.system.models;

public enum ParkingStatus {

    ACTIVE("active"), PAID("paid"), LOST("lost");

    private String parkingStatus;

    private ParkingStatus(String parkingStatus) {
        this.parkingStatus = parkingStatus;
    }

    public String getParkingStatus() {
        return parkingStatus;
    }
}
