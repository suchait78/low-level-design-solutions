package parking.lot.system.models;

public class Customer {

    private String customerName;
    private String customerVehicleNumber;
    private VehicleSupported vehicleType;

    public Customer(String customerName, String customerVehicleNumber, VehicleSupported vehicleType) {
        this.customerName = customerName;
        this.customerVehicleNumber = customerVehicleNumber;
        this.vehicleType = vehicleType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerVehicleNumber() {
        return customerVehicleNumber;
    }

    public void setCustomerVehicleNumber(String customerVehicleNumber) {
        this.customerVehicleNumber = customerVehicleNumber;
    }

    public VehicleSupported getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleSupported vehicleType) {
        this.vehicleType = vehicleType;
    }
}
