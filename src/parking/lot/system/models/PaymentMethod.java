package parking.lot.system.models;

public enum PaymentMethod {

    CARD("card"), CASH("cash");

    private String paymentMethod;

    private PaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }
}
