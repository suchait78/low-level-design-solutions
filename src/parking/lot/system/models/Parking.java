package parking.lot.system.models;

public class Parking {

    private long parkingId;
    private Customer customer;
    private BillingLocations billingLocation;
    private PaymentMethod paymentMethod;
    private double totalActiveHours;
    private SpotType spotType;
    private ParkingStatus parkingStatus;
    private long totalAmount;
    private Attendant attendant;
    private boolean isBillPaid;
    private ParkingFloor parkingFloor;

    public Parking(Customer customer, SpotType spotType, ParkingStatus parkingStatus) {

        this.parkingId = (long) Math.random();
        this.customer = customer;
        this.spotType = spotType;
        this.parkingStatus = parkingStatus;
        this.isBillPaid = false;
    }

    public ParkingFloor getParkingFloor() {
        return parkingFloor;
    }

    public void setParkingFloor(ParkingFloor parkingFloor) {
        this.parkingFloor = parkingFloor;
    }

    public boolean isBillPaid() {
        return isBillPaid;
    }

    public void setBillPaid(boolean billPaid) {
        isBillPaid = billPaid;
    }

    public long getParkingId() {
        return parkingId;
    }

    public Attendant getAttendant() {
        return attendant;
    }

    public void setAttendant(Attendant attendant) {
        this.attendant = attendant;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BillingLocations getBillingLocation() {
        return billingLocation;
    }

    public void setBillingLocation(BillingLocations billingLocation) {
        this.billingLocation = billingLocation;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public double getTotalActiveHours() {
        return totalActiveHours;
    }

    public void setTotalActiveHours(double totalActiveHours) {
        this.totalActiveHours = totalActiveHours;
    }

    public SpotType getSpotType() {
        return spotType;
    }

    public void setSpotType(SpotType spotType) {
        this.spotType = spotType;
    }

    public ParkingStatus getParkingStatus() {
        return parkingStatus;
    }

    public void setParkingStatus(ParkingStatus parkingStatus) {
        this.parkingStatus = parkingStatus;
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "Parking{" +
                "parkingId=" + parkingId +
                ", customer=" + customer +
                ", billingLocation=" + billingLocation +
                ", paymentMethod=" + paymentMethod +
                ", totalActiveHours=" + totalActiveHours +
                ", spotType=" + spotType +
                ", parkingStatus=" + parkingStatus +
                ", totalAmount=" + totalAmount +
                ", attendant=" + attendant +
                ", isBillPaid=" + isBillPaid +
                ", parkingFloor=" + parkingFloor +
                '}';
    }
}
