package parking.lot.system.command;

import parking.lot.system.exception.ParkingException;
import parking.lot.system.models.*;

import java.util.Map;

public class ParkingEntryCommand implements Command {

    private Customer customer;
    private SpotType spotType;
    private Attendant attendant;
    private ParkingLot parkingLot;

    public ParkingEntryCommand(Customer customer, SpotType spotType,
                               Attendant attendant, ParkingLot parkingLot) {
        this.customer = customer;
        this.spotType = spotType;
        this.attendant = attendant;
        this.parkingLot = parkingLot;
    }

    @Override
    public Parking execute() throws ParkingException {

        System.out.println("Issuing parking to customer.");

        if (parkingLot.getTotalParkedVehicles() + 1 > parkingLot.getTotalParkingCapacity()) {
            throw new ParkingException("Parking is full - sorry we can't allow anymore entry.");
        }

        Parking parking = new Parking(customer, spotType, ParkingStatus.ACTIVE);

        long currentActiveParkingFloorNumber = parkingLot.getCurrentActiveParkingNumber();
        ParkingFloor parkingFloor = parkingLot.getCurrentParkingFloor(currentActiveParkingFloorNumber);

        if (parkingFloor.getTotalActiveVehicleEntries() + 1 > parkingFloor.getTotalParkingCapacity()) {

            System.out.println("ParkingFloor " + currentActiveParkingFloorNumber +
                    " is full so assigning another floor.");

            synchronized (this) {
                currentActiveParkingFloorNumber++;
            }

            if (currentActiveParkingFloorNumber > parkingLot.getTotalParkingFloors()) {

                throw new ParkingException("We don't next parking floor - so we can't allow anymore.");
            }

            synchronized (this) {
                parkingLot.setCurrentActiveParkingNumber(currentActiveParkingFloorNumber);
            }
        }

        Map.Entry<Spot, Long> currentSpot = parkingFloor.getEntryRemainingSpot(spotType);

        if (currentSpot.getValue() + 1 > currentSpot.getKey().getTotalSpotsInParkingFloor()) {

            System.out.print(" Spot type " + spotType + "is full in current parking floor so " +
                    " assigning new parking floor");

            synchronized (this) {
                currentActiveParkingFloorNumber++;
            }

            if (currentActiveParkingFloorNumber > parkingLot.getTotalParkingFloors()) {

                throw new ParkingException("We don't next parking floor - so we can't allow anymore.");
            }

            synchronized (this) {
                parkingLot.setCurrentActiveParkingNumber(currentActiveParkingFloorNumber);
            }

        }

        synchronized (this) {

            parkingFloor.setTotalActiveVehicleEntries(parkingFloor.getTotalActiveVehicleEntries() + 1);
            parkingFloor.setTotalVacantVehicleEntries(parkingFloor.getTotalVacantVehicleEntries() - 1);
            parkingFloor.addParkingInParkingList(parking);

            parkingFloor.addSpotInTotalSpotsMap(currentSpot.getKey(),currentSpot.getValue() + 1);

        }

        if (!parkingFloor.getAttendantList().contains(attendant)) {
            throw new ParkingException("Wrong attendant entry.");
        }

        parking.setParkingFloor(parkingFloor);
        parking.setAttendant(attendant);
        attendant.addParkingToAttendantParkingProcessedList(parking);

        synchronized (this) {
            parkingLot.setTotalParkedVehicles(parkingLot.getTotalParkedVehicles() + 1);
        }

        return parking;
    }
}
