package parking.lot.system.command;

import parking.lot.system.models.Parking;
import parking.lot.system.exception.ParkingException;

public interface Command {

    public Parking execute() throws ParkingException;
}
