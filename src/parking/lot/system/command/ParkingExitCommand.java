package parking.lot.system.command;

import parking.lot.system.exception.ParkingException;
import parking.lot.system.models.*;

public class ParkingExitCommand implements Command {

    private Parking parking;
    private ParkingFloor parkingFloor;
    private ParkingLot parkingLot;
    private BillingLocations billingLocation;
    private PaymentMethod paymentMethod;

    public ParkingExitCommand(Parking parking, ParkingLot parkingLot,
                              BillingLocations billingLocation, PaymentMethod paymentMethod) {

        this.parking = parking;
        this.parkingFloor = parking.getParkingFloor();
        this.parkingLot = parkingLot;
        this.billingLocation = billingLocation;
        this.paymentMethod = paymentMethod;
    }

    @Override
    public Parking execute() throws ParkingException {

        parking.setBillingLocation(billingLocation);
        parking.setPaymentMethod(paymentMethod);
        parking.setParkingStatus(ParkingStatus.PAID);

        synchronized (this) {
            parkingFloor.setTotalActiveVehicleEntries(parkingFloor.getTotalActiveVehicleEntries() - 1);
            parkingFloor.setTotalVacantVehicleEntries(parkingFloor.getTotalVacantVehicleEntries() + 1);
            parkingLot.setTotalParkedVehicles(parkingLot.getTotalParkedVehicles() - 1);
        }

        Attendant attendant = parking.getAttendant();
        attendant.addParkingToAttendantParkingProcessedList(parking);

        return parking;
    }
}
