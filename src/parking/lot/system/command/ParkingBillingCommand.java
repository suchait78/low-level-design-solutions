package parking.lot.system.command;

import parking.lot.system.models.Parking;
import parking.lot.system.exception.ParkingException;
import parking.lot.system.models.VehicleSupported;

public class ParkingBillingCommand implements Command {

    private Parking parking;
    private double totalActiveParkingHours;

    public ParkingBillingCommand(Parking parking, long totalActiveParkingHours) {
        this.parking = parking;
        this.totalActiveParkingHours = totalActiveParkingHours;
    }

    @Override
    public Parking execute() throws ParkingException {

        VehicleSupported vehicle = parking.getCustomer().getVehicleType();
        parking.setTotalActiveHours(totalActiveParkingHours);
        parking.setTotalAmount((long)totalActiveParkingHours * vehicle.getParkingPrice());

        return parking;
    }
}
