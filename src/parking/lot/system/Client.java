package parking.lot.system;

import parking.lot.system.command.ParkingBillingCommand;
import parking.lot.system.command.ParkingEntryCommand;
import parking.lot.system.command.ParkingExitCommand;
import parking.lot.system.exception.ParkingException;
import parking.lot.system.models.*;

public class Client {

    public static void main(String[] args) throws ParkingException {

        ParkingLot parkingLot = new ParkingLot(2);
        ParkingFloor parkingFloor1 = new ParkingFloor(1, 5);
        ParkingFloor parkingFloor2 = new ParkingFloor(2, 5);

        Attendant attendant1 = new Attendant(122, "test1");
        Attendant attendant2 = new Attendant(123, "test2");
        Attendant attendant3 = new Attendant(124, "test3");
        Attendant attendant4 = new Attendant(125, "test4");

        parkingFloor1.addAttendantToAttenantList(attendant1);
        parkingFloor1.addAttendantToAttenantList(attendant2);

        parkingFloor1.addSpotInTotalSpotsMap(new Spot(SpotType.COMPACT, 2), 0L);
        parkingFloor1.addSpotInTotalSpotsMap(new Spot(SpotType.MOTOR_CYCLE, 1), 0L);
        parkingFloor1.addSpotInTotalSpotsMap(new Spot(SpotType.ELECTRIC, 1), 0L);
        parkingFloor1.addSpotInTotalSpotsMap(new Spot(SpotType.LARGE, 1), 0L);
        parkingFloor1.addSpotInTotalSpotsMap(new Spot(SpotType.HANDICAPPED, 1), 0L);


        parkingFloor2.addAttendantToAttenantList(attendant3);
        parkingFloor2.addAttendantToAttenantList(attendant4);

        parkingFloor2.addSpotInTotalSpotsMap(new Spot(SpotType.COMPACT, 2), 0L);
        parkingFloor2.addSpotInTotalSpotsMap(new Spot(SpotType.MOTOR_CYCLE, 1), 0L);
        parkingFloor2.addSpotInTotalSpotsMap(new Spot(SpotType.ELECTRIC, 1), 0L);
        parkingFloor2.addSpotInTotalSpotsMap(new Spot(SpotType.LARGE, 1), 0L);
        parkingFloor2.addSpotInTotalSpotsMap(new Spot(SpotType.HANDICAPPED, 1), 0L);


        parkingLot.addParkingFloorInTotalParkingFloorsMap(1L, parkingFloor1);
        parkingLot.addParkingFloorInTotalParkingFloorsMap(2L, parkingFloor2);


        Customer customer1 = new Customer("customer1","VEHICLE123",VehicleSupported.BIKE);

        ParkingEntryCommand entryCommand1 =
                new ParkingEntryCommand(customer1,SpotType.MOTOR_CYCLE,attendant1,parkingLot);

        Parking parking = entryCommand1.execute();

        ParkingBillingCommand billingCommand1 =
                new ParkingBillingCommand(parking, 2);

        parking = billingCommand1.execute();

        System.out.println("Total Billed Amount : " + parking.getTotalAmount());

        parking.setBillPaid(true);

        if(parking.isBillPaid()) {

            ParkingExitCommand exitCommand1 =
                    new ParkingExitCommand(parking, parkingLot, BillingLocations.EXIT, PaymentMethod.CASH);

            parking = exitCommand1.execute();

            System.out.println(parking);
        }

    }
}
