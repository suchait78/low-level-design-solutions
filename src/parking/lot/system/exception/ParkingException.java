package parking.lot.system.exception;

public class ParkingException extends Exception {

    private String exceptionMessage;

    public ParkingException(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

}
