Approach to solve OOD problems - 

1. Find out what is the problem ?
2. Find out all entities involved in this problem ? - this should get convered as part of model classes
3. What are the features ? - This should get covered as part of functional classes.
    * Flow of a feature.
4. Find out all model classes.
5. Find out all functional classes.
6. Find out if we can apply any design pattern - Based on that decide interfaces and few other classes.
7. Find out where is the scope of exception handling
8. Find out what all resources are shared resources is there any scope of thread safety.
9. Code optimization - Algorithmic
10. Think about different use cases.